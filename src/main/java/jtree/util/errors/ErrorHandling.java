package jtree.util.errors;

import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.function.Consumer;

public final class ErrorHandling {

    private ErrorHandling() {
        throw new AssertionError("This is a static utility class.");
    }

    public static <T> Optional<T> attempt(Callable<T> callable, Consumer<Exception> onError) {
        try {
            return Optional.of(callable.call());
        } catch (Exception e) {
            onError.accept(e);
            return Optional.empty();
        }
    }

}