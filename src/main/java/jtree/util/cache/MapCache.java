package jtree.util.cache;

import java.util.Iterator;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * Basic in-memory cache.
 */
class MapCache<K,V> implements Cache<K,V> {

    private final Map<K,V> base;
    private final Function<K,V> loader;

    public MapCache(Map<K, V> base, Function<K, V> loader) {
        this.base = base;
        this.loader = loader;
    }

    @Override
    public V get(K key) {
        final V value = base.get(key);
        return value == null ? load(key) : value;
    }

    private V load(K key) {
        final V newEntry = loader.apply(key);
        base.put(key, newEntry);
        return newEntry;
    }

    @Override
    public V put(K key, V value) {
        return base.put(key, value);
    }

    @Override
    public V remove(K key) {
        return base.remove(key);
    }

    @Override
    public Iterator<Map.Entry<K, V>> iterator() {
        return base.entrySet().iterator();
    }

    @Override
    public void forEach(BiConsumer<? super K, ? super V> action) {
        base.forEach(action);
    }

    @Override
    public boolean isEmpty() {
        return base.isEmpty();
    }

}
