package jtree.util.cache;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;

public interface Cache<K,V> extends Iterable<Map.Entry<K, V>> {

    V get(K key);

    V put(K key, V value);

    V remove(K key);

    boolean isEmpty();

    void forEach(BiConsumer<? super K, ? super V> biConsumer);

    /**
     * Create a simple in-memory cache that uses the given function to load new items.
     */
    static <K,V> Cache<K,V> create(Function<K, V> loader) {
        return new MapCache<>(new HashMap<>(), loader);
    }

}
