package jtree.util.data;

import java.util.function.Consumer;

public final class Shorthands {

    private Shorthands() {
        throw new AssertionError("This is a static utility class.");
    }

    public static <T> T then(T t, Runnable after) {
        try {
            return t;
        } finally {
            after.run();
        }
    }

    public static <T> T apply(T t, Consumer<? super T> consumer) {
        consumer.accept(t);
        return t;
    }

    public static <T> Consumer<T> skip() {
        return t -> {};
    }

}
