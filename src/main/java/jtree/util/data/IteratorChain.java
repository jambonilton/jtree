package jtree.util.data;

import java.util.Collections;
import java.util.Iterator;

public class IteratorChain<E> implements Iterator<E> {

    final Iterator<Iterator<? extends E>> iterators;
    Iterator<? extends E> current;

    public IteratorChain(Iterator<Iterator<? extends E>> iterators) {
        this.iterators = iterators;
        this.current = Collections.emptyIterator();
    }

    @Override
    public boolean hasNext() {
        return current.hasNext() || hasNextIterator();
    }

    private boolean hasNextIterator() {
        while (!current.hasNext() && iterators.hasNext())
            current = iterators.next();
        return current.hasNext();
    }

    @Override
    public E next() {
        return current.next();
    }
}
