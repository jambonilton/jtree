package jtree.util.data.plot;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Like a map, but with two keys.
 * TODO extend Map<Entry<Kx,Ky>, V>?
 */
public interface Plot<Kx, Ky, V> {

    Optional<V> get(Kx x, Ky y);

    Optional<V> put(Kx x, Ky y, V value);

    Stream<Map.Entry<Ky, V>> alongX(Kx x);

    Stream<Map.Entry<Kx, V>> alongY(Ky y);

}
