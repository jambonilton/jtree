package jtree.util.data.plot;

import jtree.util.data.Pair;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

public class XPlot<Kx,Ky,V> implements Plot<Kx,Ky,V> {

    protected final Ky y;
    protected final Map<Kx,V> map;

    public XPlot(Ky y) {
        this(y, new HashMap<>());
    }

    public XPlot(Ky y, Map<Kx, V> map) {
        this.y = y;
        this.map = map;
    }

    @Override
    public Optional<V> get(Kx x, Ky y) {
        if (!this.y.equals(y))
            return Optional.empty();
        return Optional.ofNullable(map.get(x));
    }

    @Override
    public Optional<V> put(Kx x, Ky y, V value) {
        if (!this.y.equals(y))
            throw new UnsupportedOperationException("Cannot assign Y value in XPlot.");
        return Optional.ofNullable(map.put(x, value));
    }

    @Override
    public Stream<Map.Entry<Ky, V>> alongX(Kx x) {
        if (!map.containsKey(x))
            return Stream.empty();
        return Stream.of(new Pair<>(y, map.get(x)));
    }

    @Override
    public Stream<Map.Entry<Kx, V>> alongY(Ky y) {
        if (!this.y.equals(y))
            return Stream.empty();
        return map.entrySet().stream();
    }
}
