package jtree.util.data.plot;

import jtree.util.data.Pair;
import jtree.util.data.Triple;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * A Plot implementation with a List of Triples as its internal storage.  Useful for small data sets where a retrieval
 * time of O(n) is not a concern.
 */
public class ListPlot<Kx, Ky, V> implements Plot<Kx, Ky, V> {

    protected final List<Triple<Kx, Ky, V>> entries;

    public ListPlot() {
        this(new ArrayList<>());
    }

    public ListPlot(List<Triple<Kx, Ky, V>> entries) {
        this.entries = entries;
    }

    @Override
    public Optional<V> get(Kx x, Ky y) {
        return findTriple(x, y)
                .map(Triple::getValue);
    }

    @Override
    public Optional<V> put(Kx x, Ky y, V value) {
        final Optional<Triple<Kx, Ky, V>> triple = findTriple(x, y);
        if (triple.isPresent())
            return Optional.of(triple.get().setValue(value));
        entries.add(new Triple<>(x, y, value));
        return Optional.empty();
    }

    @Override
    public Stream<Map.Entry<Ky, V>> alongX(Kx x) {
        return entries.parallelStream()
                .filter(e -> e.getX().equals(x))
                .map(triple -> new Pair<>(triple.getY(), triple.getValue()));
    }

    @Override
    public Stream<Map.Entry<Kx, V>> alongY(Ky y) {
        return entries.parallelStream()
                .filter(e -> e.getY().equals(y))
                .map(triple -> new Pair<>(triple.getX(), triple.getValue()));
    }

    private Optional<Triple<Kx, Ky, V>> findTriple(Kx x, Ky y) {
        return entries.parallelStream()
                .filter(e -> e.getX().equals(x) && e.getY().equals(y))
                .findAny();
    }

}
