package jtree.util.data;

import jtree.util.io.IOConsumer;
import jtree.util.io.Loadable;
import jtree.util.io.WriteException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static java.util.Collections.emptyIterator;
import static jtree.util.data.Shorthands.skip;

public final class Iterables {

    /**
     * Rather than resetting the index on every new call to iterator(), this will continue from the position of the
     * previous iterator, effectively "cycling" through the collection when called multiple times.
     */
    public static <E> Iterable<E> cycling(Collection<E> base) {
        return new CyclingIterable<>(base);
    }

    /**
     * Takes a single-use iterator and turns it into multi-use by maintaining records in a list for future iteration.
     */
    public static <E> LoadingIterable<E> recalling(Iterator<E> iterator) {
        return new LoadingIterable<>(iterator);
    }

    /**
     * Limit the amount of results to iterate through.
     */
    public static <E> Iterator<E> bounded(Iterator<E> iterator, int limit) {
        return new BoundedIterator(iterator, limit);
    }

    /**
     * Given some iterators, treat them as a single one, with each leading into the next.
     */
    public static <E> Iterator<E> concat(Iterator<? extends E>... iterators) {
        return new IteratorChain<>(new ArrayIterator<>(iterators));
    }

    public static <E> Iterator<E> concat(Iterable<? extends E>... iterables) {
        return new IteratorChain(Stream.of(iterables)
                .map(Iterable::iterator).iterator());
    }

    public static <I,O> Iterator<O> map(Iterator<I> in, Function<? super I, O> f) {
        return new Iterator<O>() {
            @Override
            public boolean hasNext() {
                return in.hasNext();
            }
            @Override
            public O next() {
                return f.apply(in.next());
            }
        };
    }

    public static <E> Iterator<E> loop(Iterable<E> iterable) {
        return Stream.generate(() -> iterable).flatMap(Streams::toStream).iterator();
    }

    /**
     * Apply a side-effect to whenever next() is called.
     */
    public static <E> Iterator<E> onNext(Iterator<E> iterator, Consumer<? super E> consumer) {
        return new Iterator<E>() {
            @Override
            public boolean hasNext() {
                return iterator.hasNext();
            }
            @Override
            public E next() {
                final E next = iterator.next();
                consumer.accept(next);
                return next;
            }
        };
    }

    /**
     * Applies a side-effect when the iterator has been consumed (hasNext() returns false).
     */
    public static <E> Iterator<E> whenConsumed(Iterator<E> iterator, Runnable onConsumed) {
        return new Iterator<E>() {

            boolean onConsumedCalled = false;

            @Override
            public boolean hasNext() {
                return !onConsumedCalled && checkNext();
            }

            private boolean checkNext() {
                return iterator.hasNext() ? true : applyConsumer();
            }

            private boolean applyConsumer() {
                onConsumed.run();
                return !(onConsumedCalled = true);
            }

            @Override
            public E next() {
                return iterator.next();
            }

        };
    }

    /**
     * Retain only elements matching predicate.
     */
    public static <E> Iterator<E> filter(Iterator<E> iterator, Predicate<? super E> predicate) {
        return Streams.toStream(iterator).filter(predicate).iterator();
    }

    public static <E> Iterable<E> filter(Iterable<E> iterable, Predicate<? super E> predicate) {
        return () -> filter(iterable.iterator(), predicate);
    }

    /**
     * Joins string iterable by concatenated with provided delimiter between elements.
     */
    public static void join(Iterator<IOConsumer<Appendable>> items, String delimiter, Appendable out) {
        if (!items.hasNext())
            return;
        try {
            items.next().accept(out);
            while (items.hasNext())
                items.next().accept(out.append(delimiter));
        } catch (IOException e) {
            throw new WriteException(e);
        }
    }

    /**
     * Create an iterator for a single item.
     */
    public static <E> Iterator<E> forItem(E item) {
        return new Iterator<E>() {
            boolean hasNext = true;
            @Override
            public boolean hasNext() {
                return hasNext;
            }
            @Override
            public E next() {
                hasNext = false;
                return item;
            }
        };
    }

    public static boolean hasExactlyOneElement(Iterable<?> iterable) {
        final Iterator<?> iterator = iterable.iterator();
        if (!iterator.hasNext())
            return false;
        iterator.next();
        return !iterator.hasNext();
    }

    public static class LoadingIterable<E> implements Iterable<E>, Loadable {

        protected final Collection<E> memory;
        protected final Iterator<E> base;

        public LoadingIterable(Iterator<E> base) {
            this.memory = new ArrayList<>();
            this.base = onNext(base, memory::add);
        }

        @Override
        public Iterator<E> iterator() {
            return base.hasNext()
                    ? concat(new ArrayList<>(memory).iterator(), base)
                    : memory.iterator();
        }

        @Override
        public void load() {
            base.forEachRemaining(skip());
        }

    }

    public static class CyclingIterable<E> implements Iterable<E> {

        final Collection<E> collection;
        Iterator<E> current;

        public CyclingIterable(Collection<E> collection) {
            this.collection = collection;
        }

        @Override
        public Iterator<E> iterator() {
            if (current == null || !current.hasNext())
                current = collection.iterator();
            return bounded(concat(current, collection.iterator()), collection.size());
        }

    }

    public static class BoundedIterator<E> implements Iterator<E> {

        final Iterator<E> base;
        final int limit;

        int count=0;

        public BoundedIterator(Iterator<E> base, int limit) {
            this.base = base;
            this.limit = limit;
        }

        @Override
        public boolean hasNext() {
            return base.hasNext() && count < limit;
        }

        @Override
        public E next() {
            count++;
            return base.next();
        }
    }

}
