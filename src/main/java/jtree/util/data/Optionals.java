package jtree.util.data;

import java.util.Optional;
import java.util.function.Predicate;

public final class Optionals {

    public Optionals() {
        throw new AssertionError("This is a static utility class.");
    }

    public static <T> Optional<T> ifSatisfied(T obj, Predicate<? super T> test) {
        return test.test(obj) ? Optional.empty() : Optional.ofNullable(obj);
    }

}
