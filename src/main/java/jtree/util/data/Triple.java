package jtree.util.data;

import java.util.Map;

public class Triple<X,Y,Z> extends Pair<Map.Entry<X, Y>, Z> {

    public Triple(X x, Y y, Z z) {
        this(new Pair<>(x, y), z);
    }

    public Triple(Map.Entry<X, Y> key, Z value) {
        super(key, value);
    }

    public X getX() {
        return getKey().getKey();
    }

    public Y getY() {
        return getKey().getValue();
    }

}
