package jtree.util.data;

import java.util.Iterator;
import java.util.Optional;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public final class Streams {

    public Streams() {
        throw new AssertionError("This is a static utility class.");
    }

    public static <T> Stream<T> toStream(Iterable<? extends T> iterable) {
        return StreamSupport.stream((Spliterator<T>) iterable.spliterator(), false);
    }

    public static <T> Stream<T> toStream(Iterator<? extends T> iterator) {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator, Spliterator.ORDERED), false);
    }

    public static <T> Stream<T> toStream(Optional<? extends T> optional) {
        return optional.isPresent() ? Stream.of(optional.get()) : Stream.empty();
    }

}