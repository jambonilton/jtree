package jtree.util.data;

import jtree.util.data.plot.Plot;

import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Generic graph data structure which uses a plot to store edges between nodes.
 * @param <N> The format value type.
 * @param <E> The edge value type.
 */
public class RelationGraph<N, E> {

    protected final Plot<N, N, E> edges;

    public RelationGraph(Plot<N, N, E> edges) {
        this.edges = edges;
    }

    public Optional<E> get(N from, N to) {
        return edges.get(from, to);
    }

    public Optional<E> get(N from, N to, BinaryOperator<E> pathCombiner) {
        final Optional<E> direct = edges.get(from, to);
        if (direct.isPresent())
            return direct;
        return edges.alongX(from)
                .flatMap(navigateTo(to, new HashSet<>(), pathCombiner))
                .findAny();
    }

    // Recursively search for paths to a given format.
    // TODO BFS not DFS for shortest path
    private Function<Map.Entry<N, E>, Stream<E>> navigateTo(N to, Set<N> visited, BinaryOperator<E> pathCombiner) {
        return entry -> {
            if (entry.getKey().equals(to))
                return Stream.of(entry.getValue());
            return edges.alongX(entry.getKey())
                    .filter(next -> !visited.contains(next.getKey()))
                    .peek(next -> visited.add(next.getKey()))
                    .map(next -> new Pair<>(next.getKey(), pathCombiner.apply(entry.getValue(), next.getValue())))
                    .flatMap(navigateTo(to, visited, pathCombiner));
        };
    }

    public Optional<E> put(N x, N y, E value) {
        return edges.put(x, y, value);
    }
}
