package jtree.util.reflect;

import jtree.util.data.*;
import jtree.util.data.plot.ListPlot;
import jtree.util.data.plot.Plot;
import jtree.util.io.GenericFormat;
import jtree.util.io.json.Json;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * WOLOLOLO
 */
public final class Conversion {

    private static RelationGraph<Class, Function> converters = new RelationGraph<>(new ConversionPlot(new ListPlot<>()));
    public static <I,O> void register(Class<I> in, Class<O> out, Function<? super I, ? extends O> conversion) {
        converters.put(in, out, conversion);
    }
    static {
        register(UUID.class, String.class, UUID::toString);
        register(String.class, UUID.class, UUID::fromString);
        register(Date.class, Long.class, Date::getTime);
        register(Long.class, Date.class, Date::new);
        register(Long.class, Instant.class, Instant::ofEpochMilli);
        register(Instant.class, Long.class, Instant::toEpochMilli);
        register(String.class, Instant.class, Instant::parse);
        register(Instant.class, String.class, Instant::toString);
    }

    public static Object convertTo(Object in, Type type) {
        if (type instanceof Class)
            return convertTo(in, (Class) type);
        else if (type instanceof ParameterizedType)
            return convertTo(in, (Class) ((ParameterizedType) type).getRawType());
        throw new IllegalArgumentException("Unknown type of type "+type);
    }

    public static <O> O convertTo(Object in, Class<O> type) {
        if (type.isPrimitive())
            type = Primitives.wrap(type);
        if (in == null)
            return null;
        else if (type.isInstance(in))
            return type.cast(in);
        else if (String.class == type) // TODO override string conversions
            return type.cast(String.valueOf(in));
        else if (in instanceof String && Number.class.isAssignableFrom(type))
            return convertTo(new BigDecimal((String) in), type);
        else if (type == Integer.class && in instanceof Number)
            return type.cast(((Number) in).intValue());
        else if (type == Long.class && in instanceof Number)
            return type.cast(((Number) in).longValue());
        else if (type == Double.class && in instanceof Number)
            return type.cast(((Number) in).doubleValue());
        else if (type == Float.class && in instanceof Number)
            return type.cast(((Number) in).floatValue());
        else if (type == Byte.class && in instanceof Number)
            return type.cast(((Number) in).byteValue());
        else if (type == Boolean.class && in instanceof Number)
            return type.cast(((Number) in).intValue() != 0);
        final Class<O> out = type;
        final Optional<O> fromRegistry = converters.get(in.getClass(), type, Function::andThen)
                .map(f -> out.cast(f.apply(in)));
        if (fromRegistry.isPresent())
            return fromRegistry.get();
        throw new UnsupportedOperationException("Conversion unsupported; from "+in.getClass().getSimpleName()+" to "+out.getSimpleName());
    }

    public static class ConversionPlot implements Plot<Class, Class, Function> {

        final Plot<Class, Class, Function> base;

        public ConversionPlot(Plot<Class, Class, Function> base) {
            this.base = base;
        }

        @Override
        public Optional<Function> get(Class x, Class y) {
            return base.get(x, y);
        }

        @Override
        public Optional<Function> put(Class x, Class y, Function value) {
            return base.put(x, y, value);
        }

        @Override
        public Stream<Map.Entry<Class, Function>> alongX(Class x) {
            return Stream.of(
                Streams.toStream(Optional.ofNullable(x.getSuperclass())).map(this::typePair),
                Stream.of(x.getInterfaces()).map(this::typePair),
                base.alongX(x)
            ).reduce(Stream::concat).get();
        }

        private Map.Entry<Class, Function> typePair(Class type) {
            return new Pair<>(type, type::cast);
        }

        @Override
        public Stream<Map.Entry<Class, Function>> alongY(Class y) {
            return base.alongY(y);
        }
    }

}
