package jtree.util.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.stream.Stream;

import static java.lang.Character.toUpperCase;

public final class Fields {

    private Fields() {
        throw new AssertionError("This is a static utility class.");
    }

    public static Stream<Field> getAllFields(Class<?> type) {
        final Stream<Field> declaredFields = Stream.of(type.getDeclaredFields())
                .filter(f -> !Modifier.isStatic(f.getModifiers()));
        return type.getSuperclass().equals(Object.class) ? declaredFields : Stream.concat(getAllFields(type.getSuperclass()), declaredFields);
    }

    /**
     * Attempts to set field value with setter (with convention). If unsuccessful, it will set it directly.
     */
    public static void setValue(Object instance, Field field, Object value) {
        try {
            instance.getClass().getMethod(getSetterName(field), field.getType()).invoke(instance, value);
        } catch (ReflectiveOperationException e) {
            try {
                field.setAccessible(true);
                field.set(instance, value);
            } catch (ReflectiveOperationException e2) {
                throw new FieldAccessException(e);
            }
        }
    }

    private static String getSetterName(Field field) {
        final String name = field.getName();
        return "set" + toUpperCase(name.charAt(0)) + name.substring(1);
    }

    /**
     * Attempts to get the field from the getter (with convention).  If
     * unsuccessful, it will get it from the field directly.
     */
    public static Object getValue(Object instance, Field field) {
        try {
            return instance.getClass().getMethod(getGetterName(field)).invoke(instance);
        } catch (ReflectiveOperationException e) {
            try {
                field.setAccessible(true);
                return field.get(instance);
            } catch (ReflectiveOperationException e2) {
                throw new FieldAccessException(e2);
            }
        }
    }

    /**
     * Alias for getValue(Field, Object), but without knowledge of the Field.
     */
    public static Object getValue(Object instance, String field) {
        try {
            return getValue(instance, instance.getClass().getField(field));
        } catch (ReflectiveOperationException e) {
            throw new FieldAccessException(e);
        }
    }

    private static String getGetterName(Field field) {
        final String name = field.getName();
        if (field.getType().equals(Boolean.class) || field.getType().equals(boolean.class))
            return "is" + toUpperCase(name.charAt(0)) + name.substring(1);
        return "get" + toUpperCase(name.charAt(0)) + name.substring(1);
    }

    public static class FieldAccessException extends RuntimeException {

        public FieldAccessException(Throwable e) {
            super(e);
        }

    }

}
