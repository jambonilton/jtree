package jtree.util.reflect;

import java.lang.reflect.Array;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;

public final class Defaults {

    private Defaults() {
        throw new AssertionError("This is a static utility class.");
    }

    private static final Map<Class<?>, Object> DEFAULT_VALUES = Stream
            .of(boolean.class, byte.class, char.class, double.class, float.class, int.class, long.class, short.class)
            .collect(toMap(clazz -> (Class<?>) clazz, clazz -> Array.get(Array.newInstance(clazz, 1), 0)));

    public static <T> T getDefault(Class<? extends T> type) {
        return (T) DEFAULT_VALUES.get(type);
    }

}
