package jtree.util.reflect.nodes;

import jtree.graph.NamedNode;
import jtree.graph.TreeNode;
import jtree.graph.ValueNode;
import jtree.graph.nodes.NamedNodeDecorator;
import jtree.graph.nodes.NamedValueNodeDecorator;
import jtree.util.cache.Cache;
import jtree.util.data.Iterables;
import jtree.util.data.Pair;
import jtree.util.data.RelationGraph;
import jtree.util.data.Streams;
import jtree.util.data.plot.ListPlot;
import jtree.util.data.plot.XPlot;
import jtree.util.reflect.Conversion;
import jtree.util.reflect.Primitives;

import java.lang.reflect.Type;
import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public final class NodeAdapters {

    private NodeAdapters() {
        throw new AssertionError("This is a static utility class.");
    }

    private static final Cache<Type, NodeAdapter> adapters = Cache.create(ReflectiveNodeAdapter::new);
    static {
        Primitives.allPrimitiveTypes().forEach(NodeAdapters::addValueAdapter);
        Primitives.allWrapperTypes().forEach(NodeAdapters::addValueAdapter);
        Stream.of(String.class, UUID.class, Instant.class).forEach(NodeAdapters::addValueAdapter);
        adapters.put(Map.Entry.class, new NodeAdapter<Map.Entry>() {
            @Override
            public Map.Entry from(TreeNode node) {
                return new Pair(((NamedNode) node).getName(), ((ValueNode) node).getValue());
            }
            @Override
            public TreeNode to(Map.Entry e) {
                return toNode(String.valueOf(e.getValue()), e.getValue());
            }
        });
    }

    public static <T> NodeAdapter<T> forType(Class<? extends T> type) {
        return adapters.get(type);
    }

    public static <T> T convert(TreeNode node, Class<? extends T> type) {
        if (List.class.isAssignableFrom(type)) // TODO
            return type.cast(node.asStreamOf(Object.class).collect(toList()));
        return (T) adapters.get(type).from(node);
    }

    public static TreeNode toNode(String name, Object value) {
        final TreeNode node = toNode(value);
        return node instanceof ValueNode
                ? new NamedValueNodeDecorator(name, (ValueNode) node)
                : new NamedNodeDecorator<>(name, node);
    }

    public static TreeNode toNode(Object value) {
        if (value == null)
            return ValueNode.NULL;
        // TODO
        else if (value instanceof Iterable)
            return () -> Streams.toStream((Iterable) value).map(NodeAdapters::toNode);
        else if (value instanceof Map)
            return () -> {
                final Stream<Map.Entry> stream = Streams.toStream(((Map) value).entrySet());
                return stream.map(e -> toNode(String.valueOf(e.getKey()), NodeAdapters.toNode(e.getValue())));
            };
        return adapters.get(value.getClass()).to(value);
    }

    public static <T> void addAdapter(Class<T> type, NodeAdapter<T> adapter) {
        adapters.put(type, adapter);
    }

    public static <T> void addValueAdapter(Class<T> type) {
        adapters.put(type, new ValueNodeAdapter<>(type));
    }

}
