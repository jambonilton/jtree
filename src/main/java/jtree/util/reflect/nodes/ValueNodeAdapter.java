package jtree.util.reflect.nodes;

import jtree.graph.TreeNode;
import jtree.graph.ValueNode;
import jtree.util.reflect.Conversion;

public class ValueNodeAdapter<T> implements NodeAdapter<T> {

    final Class<T> type;

    public ValueNodeAdapter(Class<T> type) {
        this.type = type;
    }

    @Override
    public T from(TreeNode node) {
        if (node instanceof ValueNode)
            return Conversion.convertTo(((ValueNode) node).getValue(), type);
        throw new IllegalStateException("Expected value format.");
    }

    @Override
    public TreeNode to(T t) {
        return valueNode(t);
    }

    private ValueNode<T> valueNode(T t) {
        return () -> t;
    }

}
