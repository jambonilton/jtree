package jtree.util.reflect.nodes;

import jtree.graph.TreeNode;

public interface NodeAdapter<T> {

    T from(TreeNode node);

    TreeNode to(T item);

}
