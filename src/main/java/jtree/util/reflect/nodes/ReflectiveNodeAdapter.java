package jtree.util.reflect.nodes;

import jtree.graph.TreeNode;
import jtree.util.reflect.Fields;

import java.lang.reflect.*;
import java.util.*;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.toList;
import static jtree.util.data.Iterables.cycling;
import static jtree.util.data.Streams.toStream;
import static jtree.util.errors.ErrorHandling.attempt;
import static jtree.util.reflect.Defaults.getDefault;
import static jtree.util.reflect.Fields.*;

public class ReflectiveNodeAdapter<T> implements NodeAdapter<T> {

    final Type type;
    final List<Constructor<T>> constructors;
    final List<Field> fields;

    public ReflectiveNodeAdapter(Type type) {
        this.type = type;
        final Class clazz = toClass(type);
        this.constructors = (List) asList(clazz.getConstructors());
        constructors.sort(comparingInt(Constructor::getParameterCount));
        this.fields = getAllFields(clazz).collect(toList());
    }

    private static Class toClass(Type type) {
        if (type instanceof Class)
            return (Class) type;
        else if (type instanceof ParameterizedType)
            return (Class)((ParameterizedType) type).getRawType();
        throw new IllegalArgumentException("Unknown type of type "+type);
    }

    public ReflectiveNodeAdapter(Class<T> type) {
        this.type = type;
        this.constructors = (List) asList(type.getConstructors());
        constructors.sort(comparingInt(Constructor::getParameterCount));
        this.fields = getAllFields(type).collect(toList());
    }

    @Override
    public T from(TreeNode node) {
        final Collection<Exception> errors = new ArrayList<>();
        return constructors.stream()
                .flatMap(c -> toStream(attempt(() -> construct(c, node), errors::add)))
                .findFirst()
                .orElseThrow(() -> getError(errors));
    }

    private T construct(Constructor<T> constructor, TreeNode node) throws ReflectiveOperationException {
        final Iterable<Field> fields = cycling(this.fields);
        final List<Object> args = getArgsFromNode(constructor, node, fields);
        final T instance = constructor.newInstance(args.toArray());
        fillInstanceFromNode(instance, fields, node);
        return instance;
    }

    private List<Object> getArgsFromNode(Constructor<T> constructor, TreeNode node, Iterable<Field> fields) {
        return Stream.of(constructor.getParameters())
                .map(p -> getParamValue(p, findAppropriateField(p, fields), node))
                .collect(toList());
    }

    private Object getParamValue(Parameter param, Optional<Field> field, TreeNode node) {
        final Object defaultValue = getDefault(param.getType());
        return field.map(f -> node.get(f).orElse(defaultValue))
                .orElse(defaultValue);
    }

    private Optional<Field> findAppropriateField(Parameter p, Iterable<Field> fields) {
        return toStream(fields)
                .filter(f -> f.getType().equals(p.getType()))
                .findFirst();
    }

    private void fillInstanceFromNode(T instance, Iterable<Field> fields, TreeNode node) {
        for (Field field : fields) {
            if (Objects.equals(getValue(instance, field), getDefault(field.getType())))
                node.get(field).ifPresent(value -> setValue(instance, field, value));
        }
    }

    @Override
    public TreeNode to(T item) {
        return () -> fields.stream().map(f -> NodeAdapters.toNode(f.getName(), getValue(item, f)));
    }

    private RuntimeException getError(Iterable<Exception> errors) {
        final Iterator<Exception> iterator = errors.iterator();
        return new ConstructionFailureException(type, iterator.hasNext() ? iterator.next() : null);
    }


    public static class ConstructionFailureException extends RuntimeException {

        public ConstructionFailureException(Type type, Throwable cause) {
            super("No suitable constructor found for type "+toClass(type).getName(), cause);
        }

    }

}
