package jtree.util;

public interface Assignable {

    <T> T asA(Class<? extends T> type);

}