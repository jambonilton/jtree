package jtree.util.io;

public class WriteException extends IORuntimeException {
    public WriteException(Throwable cause) {
        super(cause);
    }
}
