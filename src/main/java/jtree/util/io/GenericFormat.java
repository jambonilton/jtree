package jtree.util.io;

import java.io.Writer;

public interface GenericFormat {

    <T> Format<T> forType(Class<? extends T> type);

    default <T> String write(T obj) {
        return forType((Class<T>) obj.getClass()).write(obj);
    }

    default <T> void write(T obj, Writer writer) {
        forType((Class<T>) obj.getClass()).write(obj, writer);
    }

}
