package jtree.util.io;

import java.util.function.BiFunction;
import java.util.function.Function;

public class CommonTypeGenericFormat<T> implements GenericFormat {

    protected final Class<T> commonType;
    protected final Format<T> format;
    protected final Function<Object, T> toCommon;
    protected final BiFunction<T, Class<?>, Object> fromCommon;

    public CommonTypeGenericFormat(Class<T> commonType,
                                   Format<T> format,
                                   Function<Object, T> toCommon,
                                   BiFunction<T, Class<?>, Object> fromCommon) {
        this.commonType = commonType;
        this.format = format;
        this.toCommon = toCommon;
        this.fromCommon = fromCommon;
    }

    @Override
    public <U> Format<U> forType(Class<? extends U> type) {
        if (commonType.isAssignableFrom(type))
            return (Format<U>) format;
        return new Format<U>() {
            @Override
            public void write(U obj, Appendable out) {
                format.write(toCommon.apply(obj), out);
            }
            @Override
            public U parse(TextInput text) {
                return type.cast(fromCommon.apply(format.parse(text), type));
            }
        };
    }

}
