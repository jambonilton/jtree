package jtree.util.io;

import java.io.IOException;

public interface IOConsumer<T> {

    void accept(T item) throws IOException;

}
