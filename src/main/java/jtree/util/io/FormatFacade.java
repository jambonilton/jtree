package jtree.util.io;

public class FormatFacade<T> implements Format<T> {

    protected final Parser<T> parser;
    protected final Formatter<T> formatter;

    public FormatFacade(Parser<T> parser, Formatter<T> formatter) {
        this.parser = parser;
        this.formatter = formatter;
    }

    @Override
    public void write(T obj, Appendable out) {
        formatter.write(obj, out);
    }

    @Override
    public T parse(TextInput in) {
        return parser.parse(in);
    }

}
