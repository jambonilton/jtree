package jtree.util.io;

public interface StringParser<T> {

    T parse(String text);

}
