package jtree.util.io;

public interface Loadable {

    void load();

}
