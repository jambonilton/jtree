package jtree.util.io.parse;

import jtree.graph.TreeNode;
import jtree.util.io.Parser;
import jtree.util.io.TextInput;

import java.io.Reader;

public interface TreeParser extends Parser<TreeNode> {

}