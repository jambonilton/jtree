package jtree.util.io.parse;

import jtree.util.io.TextInput;

public class TreeParseException extends RuntimeException {

    protected TextInput input;

    public TreeParseException(String msg) {
        super(msg);
    }

    public TreeParseException(String msg, TextInput input) {
        super(msg);
        this.input = input;
    }

    public TreeParseException(Throwable cause, TextInput input) {
        super(cause);
    }

    public TreeParseException setInput(TextInput input) {
        this.input = input;
        return this;
    }

    @Override
    public String getMessage() {
        return (super.getMessage() == null ? "" : super.getMessage()) + " at index "+input.index()+", \"" + input + "\"";
    }
}
