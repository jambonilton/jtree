package jtree.util.io.json;

import jtree.graph.TreeNode;
import jtree.graph.ValueNode;
import jtree.graph.nodes.NamedNodeDecorator;
import jtree.graph.nodes.NamedValueNodeDecorator;
import jtree.util.data.Iterables;
import jtree.util.data.Streams;
import jtree.util.io.Parser;
import jtree.util.io.TextInput;
import jtree.util.io.parse.TreeParseException;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Iterator;
import java.util.stream.Stream;

import static java.lang.Character.isDigit;
import static java.lang.Character.isLetter;
import static jtree.util.data.Iterables.recalling;
import static jtree.util.data.Shorthands.then;

public class JsonParser implements Parser<TreeNode> {

    private static final int MAX_INT_DIGITS = String.valueOf(Integer.MAX_VALUE).length(),
                             MAX_LONG_DIGITS = String.valueOf(Long.MAX_VALUE).length();

    // TODO utf characters, escaped quotes
    @Override
    public TreeNode parse(TextInput text) {
        try {
            if (!text.hasNext())
                return null;
            final int peek = text.skipWhitespace().peek();
            if (peek == '"' || peek == '\'')
                return then(stringNode(text.skip(1).readUntilWithEscape((char) peek, '\\')), text::skip);
            else if (isDigit(peek) || peek == '-')
                return numberNode(text.readWhile(this::isNumeric));
            else if (peek == 'f' || peek == 't')
                return booleanNode(text.read(peek == 'f' ? 5 : 4));
            else if (peek == '[')
                return arrayNode(text.skip(1));
            else if (peek == '{')
                return objectNode(text.skip(1));
            throw new TreeParseException("Unexpected character "+((char) peek), text);
        } catch (TreeParseException e) {
            throw e.setInput(text);
        } catch (RuntimeException e) {
            throw new TreeParseException(e, text);
        }
    }

    private ValueNode<String> stringNode(String s) {
        return () -> s;
    }

    private boolean isNumeric(int ch) {
        return isDigit(ch) || ch == 'e' || ch == 'E' || ch == '.' || ch == '-' || ch == '+';
    }

    private ValueNode<Number> numberNode(String stringValue) {
        final Number number;
        if (stringValue.matches("-?\\d+")) {
            if (stringValue.length() < MAX_INT_DIGITS)
                number = new Integer(stringValue);
            else if (stringValue.length() < MAX_LONG_DIGITS)
                number = new Long(stringValue);
            else
                number = new BigInteger(stringValue);
        }
        else if (stringValue.matches("-?\\d+(?:\\.\\d+)?(?:[eE][+-]?\\d+)?")) {
            if (stringValue.length() < MAX_LONG_DIGITS) // TODO
                number = new Double(stringValue);
            else
                number = new BigDecimal(stringValue);
        }
        else {
            throw new TreeParseException("Expected number value but was \""+stringValue+"\".");
        }
        return () -> number;
    }

    private ValueNode<Boolean> booleanNode(String stringValue) {
        if (stringValue.equals("true"))
            return () -> true;
        else if (stringValue.equals("false"))
            return () -> false;
        throw new TreeParseException("Expected boolean value but was \""+stringValue+"\".");
    }

    private TreeNode arrayNode(TextInput text) {
        return new JsonArrayNode(new ArrayNodeTextIterator(text));
    }

    private TreeNode objectNode(TextInput text) {
        return new JsonObjectNode(new ObjectNodeTextIterator(text));
    }

    class JsonNode implements TreeNode {

        final Iterables.LoadingIterable<TreeNode> children;

        public JsonNode(Iterator<TreeNode> children) {
            this.children = recalling(children);
        }

        @Override
        public void load() {
            children.load();
        }

        @Override
        public Stream<TreeNode> children() {
            return Streams.toStream(children);
        }
    }

    class JsonObjectNode extends JsonNode {

        public JsonObjectNode(Iterator<TreeNode> children) {
            super(children);
        }

        @Override
        public boolean isAttributeNode() {
            return true;
        }

    }

    class JsonArrayNode extends JsonNode {

        public JsonArrayNode(Iterator<TreeNode> children) {
            super(children);
        }

        @Override
        public boolean isAttributeNode() {
            return false;
        }

    }

    abstract class JsonChildNodeTextIterator implements Iterator<TreeNode> {

        protected final TextInput text;
        private final char endChar;
        private boolean starting = true, done = false;
        private TreeNode previous = null;

        public JsonChildNodeTextIterator(TextInput text, char endChar) {
            this.text = text;
            this.endChar = endChar;
        }

        @Override
        public boolean hasNext() {
            if (done)
                return false;
            finishLoadingPrevious();
            final int peek = text.skipWhitespace().peek();
            done = peek == endChar;
            if (done)
                text.skipWhitespace().skipIgnoreCase(endChar);
            else if (!starting && peek != ',')
                throw new TreeParseException("Expected , or " + endChar + " but was " + (char) peek, text);
            return !done;
        }

        void finishLoadingPrevious() {
            if (previous != null)
                previous.load();
        }

        void advance() {
            if (text.peek() == ',')
                text.skip();
            starting = false;
        }

        TreeNode setPrevious(TreeNode node) {
            return this.previous = node;
        }

    }

    class ArrayNodeTextIterator extends JsonChildNodeTextIterator {

        public ArrayNodeTextIterator(TextInput text) {
            super(text, ']');
        }

        @Override
        public TreeNode next() {
            advance();
            return setPrevious(parse(text.skipWhitespace()));
        }

    }

    class ObjectNodeTextIterator extends JsonChildNodeTextIterator {

        public ObjectNodeTextIterator(TextInput text) {
            super(text, '}');
        }

        @Override
        public TreeNode next() {
            advance();
            final int peek = text.skipWhitespace().peek();
            final String nodeName;
            if (peek == '\'' || peek == '"')
                nodeName = then(text.skip().readUntil((char) peek), text::skip);
            else if (isLetter(peek))
                nodeName = text.readWhile(Character::isLetterOrDigit);
            else
                throw new TreeParseException("Expected letter or quote but was "+(char)peek, text);
            final TreeNode node = parse(text.skipWhitespace().skipIgnoreCase(':').skipWhitespace());
            return setPrevious(node instanceof ValueNode
                    ? new NamedValueNodeDecorator(nodeName, (ValueNode) node)
                    : new NamedNodeDecorator<>(nodeName, node));
        }

    }

}
