package jtree.util.io.json;

import jtree.graph.NamedNode;
import jtree.graph.TreeNode;
import jtree.graph.ValueNode;
import jtree.util.cache.Cache;
import jtree.util.io.IOConsumer;
import jtree.util.io.Formatter;
import jtree.util.reflect.Conversion;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;

import static jtree.util.data.Iterables.join;
import static jtree.util.data.Iterables.map;

public class JsonFormatter implements Formatter<TreeNode> {

    public void write(TreeNode node, Appendable out) {
        try {
            if (node.isValueNode())
                out.append(toValueJson(((ValueNode) node).getValue()));
            else {
                final ChildrenSummary children = node.children()
                        .filter(n -> !n.isNullNode())
                        .collect(summarize());
                if (node.isAttributeNode()) {
                    writeAttributes(children.namedNodes, out);
                }
                else {
                    out.append('[');
                    if (children.hasAttributes()) {
                        writeAttributes(children.namedNodes, out);
                        out.append(',');
                    }
                    join(map(children.unnamedNodes.iterator(), n -> o -> write(n, o)), ",", out);
                    out.append(']');
                }
            }
        } catch (Exception e) {
            throw new JsonWriteException(e);
        }
    }

    private String toValueJson(Object value) {
        if (value instanceof Number || value instanceof Boolean)
            return String.valueOf(value);
        return stringValue(Conversion.convertTo(value, String.class));
    }

    private String stringValue(String value) {
        final StringBuilder sb = new StringBuilder(value.length()+2);
        sb.append('"');
        for (int i=0; i < value.length(); i++) {
            final char c = value.charAt(i);
            if (c == '"')
                sb.append('\\');
            sb.append(c);
        }
        return sb.append('\"').toString();
    }

    private Collector<TreeNode, ChildrenSummary, ChildrenSummary> summarize() {
        return Collector.of(ChildrenSummary::new, ChildrenSummary::add, ChildrenSummary::merge, Collector.Characteristics.UNORDERED);
    }

    private void writeAttributes(Cache<String, List<NamedNode>> nodes, Appendable out) throws IOException {
        out.append('{');
        join(map(nodes.iterator(), this::entryToString), ",", out);
        out.append('}');
    }

    private IOConsumer<Appendable> entryToString(Map.Entry<String, List<NamedNode>> entry) {
        return out -> {
            final List<NamedNode> value = entry.getValue();
            out.append('"' + entry.getKey() + "\":");
            if (value.size() == 1)
                write(value.get(0), out);
            else {
                out.append('[');
                join(map(value.iterator(), n -> o -> write(n, o)), ",", out);
                out.append(']');
            }
        };
    }

    static class ChildrenSummary {

        final List<TreeNode> unnamedNodes;
        final Cache<String, List<NamedNode>> namedNodes;

        ChildrenSummary() {
            this.unnamedNodes = new ArrayList<>();
            this.namedNodes = Cache.create(key -> new ArrayList<>());
        }

        void add(TreeNode node) {
            if (node instanceof NamedNode) {
                final NamedNode named = (NamedNode) node;
                namedNodes.get(named.getName()).add(named);
            } else {
                unnamedNodes.add(node);
            }
        }

        ChildrenSummary merge(ChildrenSummary other) {
            unnamedNodes.addAll(other.unnamedNodes);
            other.namedNodes.forEach(namedNodes::put);
            return this;
        }

        boolean hasAttributes() {
            return !namedNodes.isEmpty();
        }

    }

}
