package jtree.util.io.json;

import jtree.graph.TreeNode;
import jtree.util.io.CommonTypeGenericFormat;
import jtree.util.io.FormatFacade;
import jtree.util.reflect.nodes.NodeAdapters;

public class Json extends CommonTypeGenericFormat<TreeNode> {

    public Json() {
        super(
            TreeNode.class,
            new FormatFacade<>(new JsonParser(), new JsonFormatter()),
            NodeAdapters::toNode,
            NodeAdapters::convert
        );
    }

}
