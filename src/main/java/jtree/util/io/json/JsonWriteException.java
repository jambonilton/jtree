package jtree.util.io.json;

import jtree.util.io.WriteException;

public class JsonWriteException extends WriteException {
    public JsonWriteException(Throwable e) {
        super(e);
    }
}
