package jtree.util.io;

import java.io.IOException;
import java.io.Reader;
import java.nio.CharBuffer;
import java.util.function.IntPredicate;

class BasicTextInput implements TextInput {

    final Reader reader;

    private CharBuffer buffer;
    private int charsRead = 0;
    private boolean closed;
    private Runnable onClose;

    public BasicTextInput(Reader reader, CharBuffer buffer, Runnable onClose) {
        this.reader = reader;
        this.buffer = buffer;
        this.onClose = onClose;
        buffer.limit(0);
    }

    @Override
    public int peek() {
        return buffer.get(buffer.position());
    }

    @Override
    public String peek(int nchars) {
        if (!hasRemaining(nchars))
            return peekWhile(c -> true);
        return buffer.duplicate().limit(buffer.position() + nchars).toString();
    }

    @Override
    public String peekWhile(IntPredicate p) {
        buffer.mark();
        try {
            final StringBuilder sb = new StringBuilder();
            while (buffer.hasRemaining() && p.test(peek()))
                sb.append(buffer.get());
            return sb.toString();
        } finally {
            buffer.reset();
        }
    }

    @Override
    public int read() {
        return buffer.get();
    }

    @Override
    public boolean hasNext() {
        return buffer.hasRemaining() || readBuffer();
    }

    @Override
    public String read(int nchars) {
        if (!hasRemaining(nchars))
            throw new IllegalArgumentException("Could not read "+nchars+", reached end of input.");
        try {
            return buffer.toString();
        } finally {
            advance(nchars);
        }
    }

    private void advance(int newPosition) {
        buffer.position(buffer.position() + newPosition);
    }

    private boolean hasRemaining(int nchars) {
        if (nchars > buffer.capacity())
            throw new IllegalArgumentException("Expected "+nchars+" is too large for buffer!");
        else if (buffer.position() + nchars > buffer.limit() && !readBuffer())
            return false;
        return true;
    }

    private boolean readBuffer() {
        try {
            buffer.compact();
            final int charsRead = reader.read(buffer);
            if (charsRead == -1) {
                buffer.limit(buffer.position());
                buffer.position(0);
                return false;
            }
            this.charsRead += charsRead;
            buffer.flip();
            return true;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public int index() {
        return charsRead - (buffer.limit() - buffer.position());
    }

    @Override
    public String toString() {
        return "..." + buffer.toString();
    }

    @Override
    public void close() {
        if (this.closed)
            return;
        try {
            onClose.run();
            reader.close();
            this.closed = true;
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

}
