package jtree.util.io;

import java.io.Reader;

public interface Parser<T> extends StringParser<T> {

    @Override
    default T parse(String str) {
        return parse(TextInput.wrap(str));
    }

    default T parse(Reader reader) {
        return parse(TextInput.wrap(reader));
    }

    T parse(TextInput text);

}
