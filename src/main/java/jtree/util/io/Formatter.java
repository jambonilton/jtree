package jtree.util.io;

public interface Formatter<T> {

    void write(T obj, Appendable out);

    default String write(T obj) {
        final StringBuilder sb = new StringBuilder();
        write(obj, sb);
        return sb.toString();
    }

}
