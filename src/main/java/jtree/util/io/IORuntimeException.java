package jtree.util.io;

public class IORuntimeException extends RuntimeException {
    public IORuntimeException(Throwable cause) {
        super(cause);
    }
}
