package jtree.util.io.xml;

import jtree.graph.NamedNode;
import jtree.graph.TreeNode;
import jtree.graph.ValueNode;
import jtree.graph.nodes.NamedValueNode;
import jtree.util.data.Streams;
import jtree.util.io.Parser;
import jtree.util.io.TextInput;

import java.util.Iterator;
import java.util.stream.Stream;

import static jtree.util.data.Iterables.*;
import static jtree.util.data.Shorthands.then;

public class XmlParser implements Parser<TreeNode> {

    // TODO version, DOCTYPE, ATTLIST, xml:lang
    @Override
    public TreeNode parse(TextInput text) {
        return !text.skipWhitespace().hasNext()
            ? ValueNode.NULL
            : rootNode(text);

    }

    protected TreeNode rootNode(TextInput text) {
        final Iterable<TreeNode> children = filter(recalling(new XmlRootNodeChildIterator(text)), n -> !n.isNullNode());
        return () -> Streams.toStream(children);
    }

    protected TreeNode parseNode(TextInput text) {
        if (!text.skipWhitespace().hasNext())
            return ValueNode.NULL;
        if (text.peek() == '<') {
            text.skip();
            if (text.peek() == '!') {
                if (text.peek(3).equals("!--"))
                    return parse(text.skipIgnoreCase("!--").skipUntil("-->").skip(3));
                else if (text.peek(9).equals("![CDATA["))
                    return valueNode(text.readUntil("]]>"));
            }
            return new XmlElementInputNode(text.readWhile(XmlParser::isNameCharacter), text);
        } else {
            return valueNode(text.readUntil('<'));
        }
    }

    static boolean isNameCharacter(int ch) {
        return ch != '>'
                && !Character.isWhitespace(ch)
                && ch != '/'
                && ch != '=';
    }

    static boolean isValueCharacter(int ch) {
        return isNameCharacter(ch);
    }

    static String readAttribute(TextInput text) {
        final int peek = text.skipWhitespace().peek();
        if (peek == '\'' || peek == '"')
            return then(text.skip().readUntil((char) peek), text::skip);
        return text.readWhile(XmlParser::isValueCharacter);
    }

    static ValueNode<String> valueNode(String value) {
        return () -> value;
    }

    static String closeTag(String name) {
        return "</"+name+">";
    }

    class XmlElementInputNode implements NamedNode, ValueNode<String> {

        final String name;
        final TextInput input;
        final LoadingIterable<TreeNode> attributes, childElements;
        boolean selfClosing;

        public XmlElementInputNode(String name, TextInput text) {
            this.name = name;
            this.input = text;
            this.attributes = recalling(whenConsumed(new XmlAttributeInputNodeIterator(text), this::skipCloseHead));
            this.childElements = recalling(whenConsumed(new XmlChildInputNodeIterator(name, text), this::skipCloseElement));

        }

        private void skipCloseHead() {
            if (input.skipWhitespace().peek() == '/') {
                selfClosing = true;
                input.skip();
            }
            input.skip('>');
        }

        private boolean canHaveChildren() {
            return !selfClosing;
        }

        private void skipCloseElement() {
            input.skipIgnoreCase(closeTag(name));
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public Stream<TreeNode> children() {
            return Streams.toStream(iterateChildren());
        }

        public Iterator<TreeNode> iterateChildren() {
            if (canHaveChildren())
                return concat(attributes, childElements);
            return attributes.iterator();
        }

        @Override
        public void load() {
            attributes.load();
            if (canHaveChildren())
                childElements.load();
        }

        // returns true if there is exactly one child format and no attributes
        @Override
        public boolean isAttributeNode() {
            final boolean onlyHasAttributes = attributes.iterator().hasNext() &&
                    !childElements.iterator().hasNext();
            final boolean allChildrenAreNamed = Streams.toStream(childElements)
                    .allMatch(child -> child instanceof NamedNode);
            return onlyHasAttributes || allChildrenAreNamed;
        }

        @Override
        public boolean isValueNode() {
            return !attributes.iterator().hasNext()
                    && hasExactlyOneElement(childElements)
                    && childElements.iterator().next() instanceof ValueNode;
        }

        @Override
        public String getValue() {
            return ((ValueNode<String>) childElements.iterator().next()).getValue();
        }

        @Override
        public String toString() {
            return name;
        }
    }

    class XmlRootNodeChildIterator implements Iterator<TreeNode> {

        final TextInput text;

        TreeNode previous = null;

        public XmlRootNodeChildIterator(TextInput text) {
            this.text = text;
        }

        @Override
        public boolean hasNext() {
            finishLoadingPrevious();
            return text.skipWhitespace().hasNext();
        }

        public void finishLoadingPrevious() {
            if (previous != null)
                previous.load();
        }

        @Override
        public TreeNode next() {
            return previous = parseNode(text);
        }
    }

    class XmlAttributeInputNodeIterator implements Iterator<TreeNode> {

        final TextInput text;

        public XmlAttributeInputNodeIterator(TextInput text) {
            this.text = text;
        }

        @Override
        public boolean hasNext() {
            return text.hasNext() && isNameCharacter(text.skipWhitespace().peek());
        }

        @Override
        public NamedNode next() {
            final String name = text.readWhile(XmlParser::isNameCharacter);
            final Object value = text.skipWhitespace().peek() == '='
                    ? readAttribute(text.skip())
                    : true;
            return new NamedValueNode<>(name, value);
        }

    }

    class XmlChildInputNodeIterator extends XmlRootNodeChildIterator {

        final String name;

        public XmlChildInputNodeIterator(String name, TextInput text) {
            super(text);
            this.name = name;
        }

        @Override
        public boolean hasNext() {
            return super.hasNext() && isCloseTag();
        }

        public boolean isCloseTag() {
            final String nextFew = text.skipWhitespace().peek(name.length() + 3);
            return !nextFew.equalsIgnoreCase(closeTag(name));
        }

    }

}
