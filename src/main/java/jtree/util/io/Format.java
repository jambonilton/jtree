package jtree.util.io;

public interface Format<T> extends Parser<T>, Formatter<T> {

}
