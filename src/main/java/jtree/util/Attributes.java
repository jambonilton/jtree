package jtree.util;

import java.util.Optional;

public interface Attributes {

    Optional<?> get(String field);

}
