package jtree.query;

import jtree.util.Attributes;
import jtree.util.data.Streams;
import jtree.util.reflect.nodes.NodeAdapter;
import jtree.util.reflect.nodes.NodeAdapters;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Stream;

public interface Query extends Predicate<Attributes>, Iterable<Query>, BooleanFormula<Query> {

    default <E> Predicate<E> forType(Class<E> type) {
        if (Attributes.class.isAssignableFrom(type))
            return (Predicate<E>) this;
        final NodeAdapter<E> adapter = NodeAdapters.forType(type);
        return e -> test(adapter.to(e));
    }

    @Override
    default Query and(Query query) {
        return and(this, query);
    }

    @Override
    default Query or(Query query) {
        return or(this, query);
    }


    default boolean hasChildren() { 
        return false;
    }

    default boolean isTautology() {
        return false;
    }

    default Stream<Query> stream() {
        return Streams.toStream(this);
    }
    
    static Conjunction and(Query... clauses) {
        return and(Arrays.asList(clauses));
    }
    
    static Conjunction and(Collection<Query> clauses) {
        return new Conjunction(clauses);
    }
    
    static Disjunction or(Query... clauses) {
        return or(Arrays.asList(clauses));
    }
    
    static Disjunction or(Collection<Query> clauses) {
        return new Disjunction(clauses);
    }
    
    static Tautology alwaysTrue() {
        return new Tautology();
    }
    
    static Contradiction alwaysFalse() {
        return new Contradiction();
    }

    static Comparison is(String field, Object value) {
        return new Comparison(field, Comparator.equals, value);
    }
    
    static Comparison lessThan(String field, Object value) {
        return new Comparison(field, Comparator.lessThan, value);
    }
    
    static Comparison greaterThan(String field, Object value) {
        return new Comparison(field, Comparator.greaterThan, value);
    }
    
    static Comparison in(String field, Object value) {
        return new Comparison(field, Comparator.isIn, value);
    }
    
    static Comparison contains(String field, Object value) {
        return new Comparison(field, Comparator.contains, value);
    }
    
    static Comparison compare(String field, Comparator comparator, Object value) {
        return new Comparison(field, comparator, value);
    }

    static FieldMatch isPresent(String field) {
        return new FieldMatch() {
            @Override
            public String getKey() {
                return field;
            }
            @Override
            public boolean test(Attributes attributes) {
                return attributes.get(field).map(Objects::nonNull).orElse(false);
            }
        };
    }

    static FieldMatch isNull(String field) {
        return new FieldMatch() {
            @Override
            public String getKey() {
                return field;
            }
            @Override
            public boolean test(Attributes attributes) {
                return attributes.get(field).map(Objects::isNull).orElse(true);
            }
        };
    }

    static ComparisonStub where(String field) {
        return new ComparisonStub(field);
    }

    class ComparisonStub {

        private final String field;

        public ComparisonStub(String field) {
            this.field = field;
        }

        public Comparison is(Object value) {
            return Query.is(field, value);
        }

        public Comparison lessThan(Object value) {
            return Query.lessThan(field, value);
        }

        public Comparison greaterThan(Object value) {
            return Query.greaterThan(field, value);
        }

        public Comparison in(Object value) {
            return Query.in(field, value);
        }

        public Comparison contains(Object value) {
            return Query.contains(field, value);
        }

        public Comparison compare(Comparator comparator, Object value) {
            return Query.compare(field, comparator, value);
        }

    }

}
