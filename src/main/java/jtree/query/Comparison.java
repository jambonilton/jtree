package jtree.query;

import jtree.util.Attributes;
import jtree.util.data.Iterables;
import jtree.util.data.Pair;
import jtree.util.reflect.Conversion;

import java.util.Iterator;

public class Comparison extends Pair<String,Object> implements Query, FieldMatch {

    private static final long serialVersionUID = 1L;

    final Comparator comparator;
    
    public Comparison(String k, Comparator c, Object v) {
        super(k, v);
        this.comparator = c;
    }
    
    public Comparator comparator() {
        return comparator;
    }

    @Override
    public boolean test(Attributes t) {
        return t.get(key)
                .map(this::testValue)
                .orElse(this.value == null);
    }

    private synchronized boolean testValue(Object value) {
        // assign comparator value to the external data type so conversion is less likely required for future tests
        try {
            if (value != null)
                setValue(Conversion.convertTo(getValue(), value.getClass()));
        } catch (Exception e) {
            // if types are incompatible, then it is assumed false
            return false;
        }
        return comparator.compare.apply(value, getValue());
    }

    @Override
    public String toString() {
        final Object value = getValue();
        return getKey() + ' ' + comparator + " "
                + (value instanceof Number || value instanceof Boolean
                    ? value
                    : '\''+String.valueOf(value)+'\'');
    }

}
