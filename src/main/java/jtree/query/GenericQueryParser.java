package jtree.query;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.joining;
import static jtree.query.Query.compare;
import static jtree.query.Query.isNull;
import static jtree.query.Query.isPresent;

public class GenericQueryParser implements QueryParser {

    protected final QueryLanguage language;
    protected final Pattern joinPattern, comparatorPattern;

    public GenericQueryParser(QueryLanguage language) {
        this.language = language;
        this.joinPattern = toPattern(Stream.of(LogicalJoin.values()).map(language::toString));
        this.comparatorPattern = toPattern(Stream.of(Comparator.values()).map(language::toString));
    }

    private Pattern toPattern(Stream<String> matchingStrings) {
        final String regex = "\\s*"+matchingStrings
                .distinct()
                .sorted(comparingInt(String::length).reversed())
                .map(s -> Pattern.quote(s))
                .collect(joining("\\s*|\\s*"))+"\\s*";
        return Pattern.compile(regex);
    }

    @Override
    public Query parse(String text) {
        final Matcher joinMatch = joinPattern.matcher(text);
        if (!joinMatch.find())
            return getComparison(text);
        int previous = 0;
        Query query = getComparison(text.subSequence(previous, joinMatch.start()));
        LogicalJoin join = language.toJoin(joinMatch.group().trim());
        previous = joinMatch.end();
        while (joinMatch.find()) {
            query = query.join(join, getComparison(text.subSequence(previous, joinMatch.start())));
            join = language.toJoin(joinMatch.group().trim());
            previous = joinMatch.end();
        }
        return query.join(join, getComparison(text.subSequence(previous, text.length())));
    }

    private Query getComparison(CharSequence text) {
        final Matcher compareMatcher = comparatorPattern.matcher(text);
        if (!compareMatcher.find()) {
            Matcher matcher;
            if ((matcher = language.variable().matcher(text)).matches())
                return isPresent(matcher.group());
            else if ((matcher = language.negated(language.variable()).matcher(text)).matches())
                return isNull(matcher.group(1));
            throw new IllegalArgumentException("Expected query term but was \""+text+"\"");
        }
        final String key = text.subSequence(0, compareMatcher.start()).toString(),
                     value = text.subSequence(compareMatcher.end(), text.length()).toString();
        final Comparator comparator = language.toComparator(compareMatcher.group().trim());
        return compare(key, comparator, value);
    }

}
