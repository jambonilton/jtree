package jtree.query;

import jtree.util.Attributes;
import jtree.util.data.Iterables;

import java.util.Iterator;

/**
 * Always false query.
 */
public class Contradiction implements Query {

    @Override
    public boolean test(Attributes t) {
        return false;
    }

    @Override
    public Iterator<Query> iterator() {
        return Iterables.forItem(this);
    }

    @Override
    public Query and(Query query) {
        return this;
    }

    @Override
    public Query or(Query query) {
        return query;
    }

}
