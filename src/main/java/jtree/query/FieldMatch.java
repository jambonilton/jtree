package jtree.query;

import jtree.util.data.Iterables;

import java.util.Iterator;

public interface FieldMatch extends Query {

    String getKey();

    @Override
    default Iterator<Query> iterator() {
        return Iterables.forItem(this);
    }


}
