package jtree.query;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.function.BiFunction;

@SuppressWarnings({"unchecked","rawtypes"})
public enum Comparator implements BiFunction<Object, Object, Boolean> {
    
    equals("=", (a,b)->a.equals(b)),
    notEquals("!=", (a,b)->!a.equals(b)),
    lessThan("<", (a,b)->compare(a,b) < 0), 
    lessThanOrEqual("<=", (a,b)->compare(a,b) <= 0), 
    greaterThan(">", (a,b)->compare(a,b) > 0), 
    greaterThanOrEqual(">=", (a,b)->compare(a,b) >= 0),
    contains("=~", Comparator::contains),
    isIn("~=", (a,b)->contains(b,a));

    final String operator;
    final BiFunction<Object,Object,Boolean> compare;
    
    Comparator(String operator,
               BiFunction<Object, Object, Boolean> compare) {
        this.operator = operator;
        this.compare = compare;
    }
    
    public Boolean apply(Object t, Object u) {
		return compare.apply(t, u);
	}

	@Override
    public String toString() {
        return operator;
    }
    
    private static Boolean contains(Object container, Object containee) {
        if (container instanceof Collection)
            return ((Collection) container).contains(containee);
        else if (container instanceof String)
            return ((String) container).contains(String.valueOf(containee));
        else if (container != null && container.getClass().isArray()) {
            for (int i = 0; i < Array.getLength(container); i++)
                if (Array.get(container, i).equals(containee))
                    return true;
            return false;
        }
        throw new IllegalArgumentException(container.getClass().getName()+" not an accepted type for contains operation!");
    }
    
    private static int compare(Object a, Object b) {
        if (a == null)
            return b == null ? 0 : -1;
        else if (b == null)
           return 1;
        if (!(a instanceof Comparable) || !(b instanceof Comparable))
            return a.equals(b) ? 0 : -1;
        return ((Comparable) a).compareTo(b);
    }
    
}
