package jtree.query;

import jtree.util.Attributes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.function.Predicate;

public class Conjunction extends ClauseListQuery {
    
    Conjunction(Query... clauses) {
        super(new ArrayList<>(Arrays.asList(clauses)));
    }
    
    Conjunction(Collection<Query> clauses) {
        super(clauses);
    }

    @Override
    public boolean test(Attributes t) {
        for (Predicate<Attributes> c : this)
            if (!c.test(t))
                return false;
        return true;
    }

    @Override
    public boolean isTautology() {
        return !hasChildren() || stream().allMatch(Query::isTautology);
    }

    @Override
    public Query and(Query query) {
        if (query instanceof Tautology)
            return this;
        else if (query instanceof Contradiction)
            return query;
        clauses.add(query);
        return this;
    }

    @Override
    public String toString() {
        return super.toString(" AND ");
    }


}
