package jtree.query;

import java.util.Collection;
import java.util.Iterator;
import java.util.stream.Collectors;

public abstract class ClauseListQuery implements Query {

    protected Collection<Query> clauses;
    
    ClauseListQuery(Collection<Query> clauses) {
        this.clauses = clauses;
    }

    @Override
    public boolean hasChildren() {
        return !clauses.isEmpty();
    }

    @Override
    public Iterator<Query> iterator() {
        return clauses.iterator();
    }

    public String toString(CharSequence join) {
        return clauses.stream().map(this::termToString).collect(Collectors.joining(join));
    }
    
    protected String termToString(Query term) {
        return term.hasChildren() ? '('+term.toString()+')' : term.toString();
    }
    
}
