package jtree.query.uri;

import jtree.query.Comparator;
import jtree.query.LogicalJoin;
import jtree.query.QueryLanguage;

import java.util.regex.Pattern;

public class QueryParamLanguage implements QueryLanguage {

    @Override
    public String toString(LogicalJoin join) {
        switch (join) {
            case AND: return "&";
            case OR: return "|";
        }
        throw new IllegalArgumentException("Unexpected toString value "+join);
    }

    @Override
    public String toString(Comparator comparator) {
        switch (comparator) {
            case equals: return "=";
            case notEquals: return "!=";
            case greaterThan: return ">";
            case greaterThanOrEqual: return ">=";
            case lessThan: return "<";
            case lessThanOrEqual: return "<=";
            case isIn: return "~=";
            case contains: return "=~";
        }
        throw new IllegalArgumentException("Unexpected comparator "+comparator);
    }

    @Override
    public Comparator toComparator(String s) {
        switch (s) {
            case "=": return Comparator.equals;
            case "!=": return Comparator.notEquals;
            case ">": return Comparator.greaterThan;
            case ">=": return Comparator.greaterThanOrEqual;
            case "<": return Comparator.lessThan;
            case "<=": return Comparator.lessThanOrEqual;
            case "~=": return Comparator.isIn;
            case "=~": return Comparator.contains;
        }
        throw new IllegalArgumentException("Unexpected comparator string "+s);
    }

    @Override
    public LogicalJoin toJoin(String s) {
        switch (s) {
            case "&": return LogicalJoin.AND;
            case "|": return LogicalJoin.OR;
        }
        throw new IllegalArgumentException("Unexpected join string "+s);
    }

    @Override
    public Pattern variable() {
        return Pattern.compile("\\w+");
    }

    @Override
    public Pattern negated(Pattern term) {
        return Pattern.compile("!("+term.pattern()+")");
    }

}
