package jtree.query;

public interface BooleanFormula<T> {

    default T join(LogicalJoin join, T other) {
        switch (join) {
            case AND: return and(other);
            case OR: return or(other);
            default: throw new UnsupportedOperationException("Cannot toString with "+join);
        }
    }
    T and(T other);
    T or(T other);
    
}
