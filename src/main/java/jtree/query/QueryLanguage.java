package jtree.query;

import java.util.regex.Pattern;

public interface QueryLanguage {

    String toString(LogicalJoin join);
    String toString(Comparator comparator);
    Comparator toComparator(String s);
    LogicalJoin toJoin(String s);
    Pattern variable();
    Pattern negated(Pattern in);

}
