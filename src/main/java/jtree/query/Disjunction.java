package jtree.query;

import jtree.util.Attributes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.function.Predicate;

public class Disjunction extends ClauseListQuery {

    Disjunction(Query... clauses) {
        super(new ArrayList<>(Arrays.asList(clauses)));
    }
    
    Disjunction(Collection<Query> clauses) {
        super(clauses);
    }

    @Override
    public boolean test(Attributes t) {
        for (Predicate<Attributes> c : this)
            if (c.test(t))
                return true;
        return false;
    }

    @Override
    public boolean isTautology() {
        return !hasChildren() || stream().anyMatch(Query::isTautology);
    }

    @Override
    public Query or(Query query) {
        if (query instanceof Tautology)
            return query;
        else if (query instanceof Contradiction)
            return this;
        this.clauses.add(query);
        return this;
    }

    @Override
    public String toString() {
        return super.toString(" OR ");
    }
    
}
