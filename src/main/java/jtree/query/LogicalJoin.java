package jtree.query;

public enum LogicalJoin {
    AND, OR
}
