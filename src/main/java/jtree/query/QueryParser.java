package jtree.query;

import jtree.util.io.StringParser;

public interface QueryParser extends StringParser<Query> {

}
