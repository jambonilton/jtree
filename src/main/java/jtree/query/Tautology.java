package jtree.query;

import jtree.util.Attributes;
import jtree.util.data.Iterables;

import java.util.Iterator;

/**
 * Always true query.
 */
public class Tautology implements Query {

    @Override
    public boolean test(Attributes t) {
        return true;
    }

    @Override
    public Iterator<Query> iterator() {
        return Iterables.forItem(this);
    }

    @Override
    public Query and(Query query) {
        return query;
    }

    @Override
    public Query or(Query query) {
        return this;
    }

    @Override
    public boolean isTautology() {
        return true;
    }
}
