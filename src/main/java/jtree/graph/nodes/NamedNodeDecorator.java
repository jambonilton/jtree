package jtree.graph.nodes;

import jtree.graph.NamedNode;
import jtree.graph.TreeNode;

import java.util.stream.Stream;

public class NamedNodeDecorator<N extends TreeNode> implements NamedNode {

    final String name;
    final N node;

    public NamedNodeDecorator(String name, N node) {
        this.name = name;
        this.node = node;
    }

    @Override
    public String getName() {
        return name;
    }

    public Stream children() {
        return node.children();
    }

    @Override
    public void load() {
        node.load();
    }

    @Override
    public boolean isAttributeNode() {
        return node.isAttributeNode();
    }

}
