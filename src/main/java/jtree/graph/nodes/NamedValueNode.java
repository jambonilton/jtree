package jtree.graph.nodes;

import jtree.graph.NamedNode;
import jtree.graph.ValueNode;

public class NamedValueNode<E> implements NamedNode, ValueNode<E> {

    protected final String name;
    protected final E value;

    public NamedValueNode(String name, E value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public E getValue() {
        return value;
    }
}
