package jtree.graph.nodes;

import jtree.graph.ValueNode;

public class NamedValueNodeDecorator extends NamedNodeDecorator<ValueNode> implements ValueNode {

    public NamedValueNodeDecorator(String name, ValueNode node) {
        super(name, node);
    }

    @Override
    public Object getValue() {
        return node.getValue();
    }

    @Override
    public void load() {
        node.load();
    }

}
