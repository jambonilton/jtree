package jtree.graph;

import jtree.graph.query.NodeQuery;
import jtree.graph.query.PathQueryParser;
import jtree.query.GenericQueryParser;
import jtree.query.uri.QueryParamLanguage;
import jtree.util.Assignable;
import jtree.util.Attributes;
import jtree.util.io.Loadable;
import jtree.util.io.Parser;
import jtree.util.io.json.JsonFormatter;
import jtree.util.io.Formatter;
import jtree.util.reflect.nodes.NodeAdapters;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.Optional;
import java.util.stream.Stream;

import static jtree.util.data.Shorthands.apply;
import static jtree.util.reflect.Conversion.convertTo;

public interface TreeNode extends Loadable, Attributes, Assignable {

    Formatter<TreeNode> JSON = new JsonFormatter();
    Parser<NodeQuery> QUERY_PARSER = new PathQueryParser(new GenericQueryParser(new QueryParamLanguage()));

    Stream<TreeNode> children();

    default Stream<TreeNode> descendents() {
        return children().flatMap(child -> Stream.concat(Stream.of(child), child.descendents()));
    }

    @Override
    default <T> T asA(Class<? extends T> type) {
        return isValueNode()
                ? convertTo(((ValueNode) this).getValue(), type)
                : NodeAdapters.convert(this, type);
    }

    default int asInt() {
        return asA(Integer.class).intValue();
    }

    @Override
    default Optional<Object> get(String field) {
        return getChild(field)
                .filter(TreeNode::isValueNode)
                .map(n -> ((ValueNode) n).getValue());
    }

    default Optional<TreeNode> getChild(String field) {
        return children()
                .filter(child -> child instanceof NamedNode && ((NamedNode) child).getName().equals(field))
                .findAny();
    }

    default Stream<TreeNode> find(String query) {
        return find(QUERY_PARSER.parse(query));
    }

    default Stream<TreeNode> find(NodeQuery query) {
        return query.apply(this);
    }

    default <T> Stream<T> asStreamOf(Class<? extends T> type) {
        return children().map(child -> child.asA(type));
    }

    default <T> T[] array(Class<T> type) {
        return asStreamOf(type).toArray(len -> (T[]) Array.newInstance(type, len));
    }

    default <T> Optional<T> getAs(String field, Class<? extends T> type) {
        return getChild(field).map(child -> child.asA(type));
    }

    default Optional<Object> get(Field field) {
        return getAs(field.getName(), field.getType());
    }

    default String toJson() {
        return apply(new StringBuilder(), sb -> toJson(sb)).toString();
    }

    default void toJson(Appendable out) {
        JSON.write(this, out);
    }

    default boolean isValueNode() {
        return this instanceof ValueNode;
    }

    default boolean isNullNode() {
        return isValueNode() && ((ValueNode) this).getValue() == null;
    }

    default boolean isAttributeNode() {
        return children().allMatch(NamedNode.class::isInstance);
    }

    // Optional
    default void load() {}

}
