package jtree.graph;

import java.util.stream.Stream;

public interface ValueNode<E> extends TreeNode {

    ValueNode NULL = () -> null;

    @Override
    default Stream<TreeNode> children() {
        return Stream.empty();
    }

    E getValue();

}
