package jtree.graph.query;

import jtree.graph.NamedNode;
import jtree.graph.TreeNode;
import jtree.query.QueryParser;
import jtree.util.io.Parser;
import jtree.util.io.TextInput;

import javax.xml.soap.Node;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static jtree.query.Query.alwaysTrue;
import static jtree.util.data.Shorthands.then;

/**
 * See README.md for syntax.
 */
public class PathQueryParser implements Parser<NodeQuery> {

    protected final QueryParser queryParser;

    public PathQueryParser(QueryParser queryParser) {
        this.queryParser = queryParser;
    }

    @Override
    public NodeQuery parse(TextInput text) {
        if (!text.hasNext())
            return Stream::of;
        return new BasicNodeQuery(
                isDescendant(text) ? TreeNode::descendents : TreeNode::children,
                parsePathTerm(text),
                parse(text));
    }

    public boolean isDescendant(TextInput text) {
        if (text.peek() == '/' && text.skip().hasNext() && text.peek() == '/') {
            text.skip();
            return true;
        }
        return false;
    }

    private Predicate<TreeNode> parsePathTerm(TextInput text) {
        Predicate<TreeNode> search = alwaysTrue().forType(TreeNode.class);
        if (!isSpecialCharacter(text.peek()))
            search = search.and(isNamed(text.readUntil(this::isSpecialCharacter)));
        if (text.hasNext() && text.peek() == '[')
            search = search.and(queryParser.parse(text.skip().readUntilSkipping(']')));
        return search;
    }

    private Predicate<TreeNode> isNamed(String name) {
        return n -> n instanceof NamedNode && ((NamedNode) n).getName().equals(name);
    }

    private boolean isSpecialCharacter(int ch) {
        return ch == '/' || ch == '[';
    }

    static class BasicNodeQuery implements NodeQuery {

        final Function<TreeNode, Stream<TreeNode>> scan;
        final Predicate<TreeNode> filter;
        final Function<TreeNode, Stream<TreeNode>> next;

        public BasicNodeQuery(Function<TreeNode, Stream<TreeNode>> scan,
                              Predicate<TreeNode> filter,
                              Function<TreeNode, Stream<TreeNode>> next) {
            this.scan = scan;
            this.filter = filter;
            this.next = next;
        }

        @Override
        public Stream<TreeNode> apply(TreeNode n) {
            return scan.apply(n)
                    .filter(filter)
                    .flatMap(next);
        }

    }

}
