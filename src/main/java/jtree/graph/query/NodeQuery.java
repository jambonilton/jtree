package jtree.graph.query;

import jtree.graph.TreeNode;

import java.util.function.Function;
import java.util.stream.Stream;

public interface NodeQuery extends Function<TreeNode, Stream<TreeNode>> {

    default TreeNode findFirst(TreeNode node) {
        return apply(node).findFirst().get();
    }

}
