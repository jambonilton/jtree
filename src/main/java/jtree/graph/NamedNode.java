package jtree.graph;

public interface NamedNode extends TreeNode {

    String getName();

}
