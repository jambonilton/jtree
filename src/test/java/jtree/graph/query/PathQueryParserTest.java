package jtree.graph.query;

import jtree.graph.NamedNode;
import jtree.graph.TreeNode;
import jtree.query.GenericQueryParser;
import jtree.query.uri.QueryParamLanguage;
import jtree.util.io.json.JsonParser;
import org.junit.Test;

import static java.util.stream.Collectors.joining;
import static org.junit.Assert.*;

public class PathQueryParserTest {

    TreeNode node = new JsonParser().parse("{a:{b:1,c:2,d:{e:3}},f:{c:5,g:6},h:{b:1,c:2},i:7}");
    PathQueryParser parser = new PathQueryParser(new GenericQueryParser(new QueryParamLanguage()));

    @Test
    public void singleTerm() {
        assertEquals(7, parser.parse("i").findFirst(node).asInt());
        assertEquals(7, parser.parse("/i").findFirst(node).asInt());
    }

    @Test
    public void descendant() {
        assertEquals(3, parser.parse("//e").findFirst(node).asInt());
    }

    @Test
    public void multi() {
        assertEquals(3, parser.parse("/a/d/e").findFirst(node).asInt());
    }

    @Test
    public void withQuery() {
        assertEquals("{\"b\":1,\"c\":2,\"d\":{\"e\":3}},{\"b\":1,\"c\":2}",
                parser.parse("[b=1]").apply(node).map(TreeNode::toJson).collect(joining(",")));
        assertEquals("{\"e\":3},{\"c\":5,\"g\":6}",
                parser.parse("//[e|g]").apply(node).map(TreeNode::toJson).collect(joining(",")));
    }

}