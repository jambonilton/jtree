package jtree.util.reflect;

import jtree.util.io.json.JsonParser;
import jtree.util.reflect.nodes.ReflectiveNodeAdapter;
import org.junit.Test;

import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

public class ReflectiveNodeAdapterTest {

    private JsonParser json = new JsonParser();
    private ReflectiveNodeAdapter<NestedObject> adapter = new ReflectiveNodeAdapter<>(NestedObject.class);

    @Test
    public void fromNodeEmptyNode() {
        final NestedObject object = adapter.from(() -> Stream.empty());
        assertEquals(0, object.id);
        assertEquals(null, object.child);
    }

    @Test
    public void fromNestedNode() {
        final NestedObject object = json.parse("{ id: 1, child: { id: 2 }}").asA(NestedObject.class);
        assertEquals(1, object.getId());
        assertEquals(2, object.getChild().id);
    }

    @Test(expected = ReflectiveNodeAdapter.ConstructionFailureException.class)
    public void fromNodeOnFailure() {
        json.parse("{ id: 'not an int' }").asA(TroublesomeObject.class);
    }

    public static class NestedObject {

        int id;
        NestedObject child;

        public NestedObject(int id, NestedObject child) {
            this.id = id;
            this.child = child;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public NestedObject getChild() {
            return child;
        }
    }

    public static class TroublesomeObject {

        int id;

        public TroublesomeObject(String id) {
            this.id = Integer.parseInt(id);
        }

        public void setId(String id) {
            this.id = Integer.parseInt(id);
        }

    }

}