package jtree.util.reflect;

import org.junit.Test;

import java.sql.Timestamp;
import java.time.Instant;

import static org.junit.Assert.*;

public class ConversionTest {

    @Test
    public void complexConversion() {
        final long time = 1505950880879L;
        final Instant instant = Conversion.convertTo(new Timestamp(time), Instant.class);
        assertEquals(time, instant.toEpochMilli());
    }

}