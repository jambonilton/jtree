package jtree.util;

import jtree.util.data.Optionals;
import jtree.util.data.Shorthands;
import jtree.util.data.Streams;
import jtree.util.errors.ErrorHandling;
import jtree.util.reflect.Defaults;
import jtree.util.reflect.Fields;
import jtree.util.reflect.Primitives;
import jtree.util.reflect.nodes.NodeAdapters;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class UtilsInstancesTest {

    @Test
    public void constructionDisabled() throws Exception {
        assertCannotConstruct(Shorthands.class, Streams.class, ErrorHandling.class,
                NodeAdapters.class, Defaults.class, Fields.class, Primitives.class, Optionals.class);
    }

    public void assertCannotConstruct(Class<?>... types) throws NoSuchMethodException, InstantiationException, IllegalAccessException {
        for (Class<?> type : types) {
            try {
                final Constructor<?> constructor = type.getDeclaredConstructor();
                constructor.setAccessible(true);
                constructor.newInstance();
                fail("Expected AssertionError for type "+type.getName());
            } catch (InvocationTargetException e) {
                assertTrue("Expected for type "+type.getName(), e.getCause() instanceof AssertionError);
            }
        }
    }

}
