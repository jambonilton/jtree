package jtree.util.cache;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MapCacheTest {

    @Test
    public void test() {
        final Cache<String, Integer> cache = Cache.create(key -> key.charAt(0) - 'a');
        assertEquals(0, cache.get("a").intValue());
        cache.put("a", 1);
        assertEquals(1, cache.get("a").intValue());
        cache.remove("a");
        assertEquals(0, cache.get("a").intValue());
    }

}