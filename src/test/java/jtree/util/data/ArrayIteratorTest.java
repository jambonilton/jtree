package jtree.util.data;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ArrayIteratorTest {

    @Test
    public void test() {
        final StringBuilder sb = new StringBuilder();
        new ArrayIterator<>(new Integer[]{1,2,3,4,5}).forEachRemaining(sb::append);
        assertEquals("12345", sb.toString());
    }

}