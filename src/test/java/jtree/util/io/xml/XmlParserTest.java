package jtree.util.io.xml;

import jtree.graph.TreeNode;
import jtree.graph.ValueNode;
import jtree.util.io.TextInput;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class XmlParserTest {

    private final XmlParser parser = new XmlParser();

    @Test
    public void empty() {
        assertEquals(ValueNode.NULL, parser.parse(""));
        assertEquals(ValueNode.NULL, parser.parse("\n"));
        assertEquals(ValueNode.NULL, parser.parseNode(TextInput.wrap("<!-- this is a test -->\n")));
    }

    @Test
    public void value() {
        final TreeNode node = parser.parse("This is a test.");
        assertEquals("[\"This is a test.\"]", node.toJson());
    }

    @Test
    public void simpleElement() {
        final TreeNode node = parser.parse("<text>This is a test.</text>");
        assertEquals("{\"text\":\"This is a test.\"}", node.toJson());
    }

    @Test
    public void list() {
        final TreeNode node = parser.parse("<ul><li>one</li><li>two</li><li>three</li></ul>");
        assertEquals("{\"ul\":{\"li\":[\"one\",\"two\",\"three\"]}}", node.toJson());
    }

    @Test
    public void attributes() {
        final TreeNode node = parser.parse("<img src=\"test.jpg\" />");
        assertEquals("{\"img\":{\"src\":\"test.jpg\"}}", node.toJson());
    }

    @Test
    public void attributesAndValues() {
        final TreeNode node = parser.parse("<a class=link href=\"some-page.html\">Check this out!</a>");
        assertEquals("{\"a\":[{\"href\":\"some-page.html\",\"class\":\"link\"},\"Check this out!\"]}", node.toJson());
    }

}