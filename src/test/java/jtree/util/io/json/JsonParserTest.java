package jtree.util.io.json;

import jtree.graph.TreeNode;
import jtree.util.io.Parser;
import jtree.util.io.TextInput;
import jtree.util.io.parse.TreeParseException;
import jtree.util.reflect.nodes.NodeAdapter;
import jtree.util.reflect.nodes.NodeAdapters;
import org.junit.Test;

import java.nio.CharBuffer;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.joining;
import static org.junit.Assert.*;

public class JsonParserTest {

    private Parser<TreeNode> parser = new Json().forType(TreeNode.class);

    @Test
    public void parsesEmpty() {
        assertNull(parser.parse(""));
    }

    @Test
    public void parsesNumbers() {
        assertEquals(0, parser.parse("0").asA(Integer.class).intValue());
        assertEquals(123, parser.parse("123").asA(Integer.class).intValue());
        assertEquals(123L, parser.parse("123").asA(Long.class).longValue());
        assertEquals(-123L, parser.parse("-123").asA(Long.class).longValue());
        assertEquals(1.23f, parser.parse("1.23").asA(Float.class).floatValue(), 0);
        assertEquals(1.23d, parser.parse("1.23").asA(Double.class).doubleValue(), 0);
        assertEquals(0.00123f, parser.parse("1.23e-3").asA(Float.class).floatValue(), 0);
        assertEquals(0.00123f, parser.parse("1.23E-3").asA(Float.class).floatValue(), 0);
        assertEquals(123f, parser.parse("1.23E+2").asA(Float.class).floatValue(), 0);
        assertEquals(1.23d, parser.parse("1.23").asA(Double.class).doubleValue(), 0);
        assertEquals(true, parser.parse("1").asA(Boolean.class));
        assertEquals(false, parser.parse("0").asA(Boolean.class));
    }

    @Test
    public void strings() {
        assertEquals("test", parser.parse("\"test\"").asA(String.class));
        assertEquals("te\"st", parser.parse("\"te\\\"st\"").asA(String.class));
        assertEquals("\"te\\\"st\"", parser.parse("\"te\\\"st\"").toJson());
    }

    @Test
    public void uuid() {
        final UUID uuid = UUID.randomUUID();
        final String input = "\"" + uuid.toString() + "\"";
        assertEquals(uuid, parser.parse(input).asA(UUID.class));
        assertEquals(input, NodeAdapters.toNode(uuid).toJson());
    }

    @Test
    public void instant() {
        final Instant instant = Instant.now();
        final String input = "\"" + instant.toString() + "\"";
        assertEquals(instant, parser.parse(input).asA(Instant.class));
        assertEquals(input, NodeAdapters.toNode(instant).toJson());
    }

    @Test(expected = TreeParseException.class)
    public void failsOnNull() {
        parser.parse((TextInput) null);
    }

    @Test(expected = TreeParseException.class)
    public void failsOnWeirdInput() {
        parser.parse("<div>this ain't no json</div>");
    }

    @Test(expected = TreeParseException.class)
    public void failsOnMalformedNumbers() {
        parser.parse("123eeee");
    }

    @Test
    public void parsesBoolean() {
        assertEquals(true, parser.parse("true").asA(Boolean.class));
        assertEquals(false, parser.parse("false").asA(Boolean.class));
    }

    @Test(expected = TreeParseException.class)
    public void parseBooleanError() {
        parser.parse("farts");
    }

    @Test
    public void parsesText() {
        assertEquals("TEST", parser.parse("\"TEST\"").asA(String.class));
        assertEquals("TEST", parser.parse("'TEST'").asA(String.class));
    }

    @Test
    public void convertsText() {
        assertEquals(0, parser.parse("'0'").asA(Integer.class).intValue());
        assertEquals(123, parser.parse("'123'").asA(Integer.class).intValue());
        assertEquals(123L, parser.parse("'123'").asA(Long.class).longValue());
        assertEquals(1.23f, parser.parse("'1.23'").asA(Float.class).floatValue(), 0);
        assertEquals(1.23d, parser.parse("'1.23'").asA(Double.class).doubleValue(), 0);
    }

    @Test
    public void arrays() {
        assertArrayEquals(new Integer[]{0,1,2,3,4},
                parser.parse("[0,1,2,3,4]").array(Integer.class));
        assertArrayEquals(IntStream.range(0,5).mapToObj(String::valueOf).toArray(String[]::new),
                parser.parse("['0','1','2','3','4']").array(String.class));
        assertArrayEquals(IntStream.range(0,5).mapToObj(String::valueOf).toArray(String[]::new),
                parser.parse("[0,1,2,3,4]").array(String.class));
    }

    @Test
    public void basicObjects() {
        final TreeNode objectNode = parser.parse("{'foo':'bar', something: 'else', age: 82}");
        assertProperty(objectNode, "age", 82);
        assertProperty(objectNode, "foo", "bar");
        assertProperty(objectNode, "something", "else");
    }

    @Test
    public void nestedObjects() {
        final TreeNode parent = parser.parse("{'nested': { 'foo':'bar', something: 'else', age: 82} }");
        final TreeNode child = parent.getChild("nested").get();
        assertProperty(child, "age", 82);
        assertProperty(child, "foo", "bar");
        assertProperty(child, "something", "else");
    }

    @Test(expected = TreeParseException.class)
    public void errorOnBadLabel() {
        final TreeNode parent = parser.parse("{'foo':'bar', `age`: 82}");
        parent.children().count();
    }

    @Test(expected = TreeParseException.class)
    public void errorOnDelimiter() {
        final TreeNode parent = parser.parse("[1, 2, 3, 4 | 5]");
        parent.children().count();
    }

    @Test
    public void arrayObjectNesting() {
        final TreeNode json = parser.parse("[{ firstName:'Steve',lastName:'Camuti',age:42, address: {} }, " +
                "{ firstName:'Bob', lastName:'Loblaw', age:52, address: {streetNumber: 123, street:\"Fake Street\"} }]");
        final Person[] people = json.array(Person.class);
        assertEquals(2, people.length);
        assertEquals("Steve", people[0].firstName);
        assertEquals("Camuti", people[0].lastName);
        assertEquals(42, people[0].age);
        assertEquals("Bob", people[1].firstName);
        assertEquals("Loblaw", people[1].lastName);
        assertEquals(52, people[1].age);
        assertEquals(123, people[1].address.getStreetNumber());
        assertEquals("Fake Street", people[1].address.getStreet());
        assertEquals(null, people[1].address.getCity());
        assertEquals("\"Steve\",\"Bob\"", json.find("//firstName").map(TreeNode::toJson).collect(joining(",")));

        assertEquals("{\"firstName\":\"Bob\",\"lastName\":\"Loblaw\",\"address\":{\"streetNumber\":123," +
                "\"street\":\"Fake Street\"},\"age\":52}", NodeAdapters.toNode(people[1]).toJson());
        assertEquals("[{\"firstName\":\"Steve\",\"lastName\":\"Camuti\",\"address\":{},\"age\":42}," +
                "{\"firstName\":\"Bob\",\"lastName\":\"Loblaw\",\"address\":{\"streetNumber\":123," +
                "\"street\":\"Fake Street\"},\"age\":52}]", json.toJson());
    }

    @Test(expected = JsonWriteException.class)
    public void throwsJsonWriteExceptionOnFailure() {
        NodeAdapters.toNode(new Person("Bob", "Loblaw", 52)).toJson(CharBuffer.allocate(0));
    }

    private static void assertProperty(TreeNode node, String field, Object expected) {
        final Optional<TreeNode> child = node.getChild(field);
        assertTrue("Expected "+field+" to be present.", child.isPresent());
        assertEquals(expected, child.get().asA(expected.getClass()));
    }

    public static class Person {

        private final String firstName;
        private final String lastName;
        private final int age;
        private Address address;

        public Person(String firstName, String lastName, int age) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.age = age;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public int getAge() {
            return age;
        }

    }

    public static class Address {

        private final int streetNumber;
        private String street;
        private String city;

        public Address(int streetNumber) {
            this.streetNumber = streetNumber;
        }

        public int getStreetNumber() {
            return streetNumber;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }
    }

}