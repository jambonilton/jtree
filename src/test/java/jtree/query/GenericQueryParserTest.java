package jtree.query;

import jtree.graph.TreeNode;
import jtree.query.uri.QueryParamLanguage;
import jtree.util.io.json.JsonParser;
import org.junit.Test;


import static org.junit.Assert.*;

public class GenericQueryParserTest {

    TreeNode node = new JsonParser().parse("{a:1,b:2,c:3,foo:'bar'}");
    QueryParser parser = new GenericQueryParser(new QueryParamLanguage());

    @Test
    public void singleTerm() {
        assertTrue(parser.parse("foo").test(node));
        assertFalse(parser.parse("bar").test(node));
        assertTrue(parser.parse("!bar").test(node));
        assertFalse(parser.parse("!foo").test(node));
        assertTrue(parser.parse("foo=bar").test(node));
        assertFalse(parser.parse("foo=war").test(node));
        assertFalse(parser.parse("too=bar").test(node));
        assertTrue(parser.parse("a > 0").test(node));
        assertTrue(parser.parse("b <= 2").test(node));
        assertTrue(parser.parse("c >= 3").test(node));
        assertTrue(parser.parse("c > -1").test(node));
        assertFalse(parser.parse("d = 0").test(node));
    }

    @Test
    public void joinedTerms() {
        assertTrue(parser.parse("foo&c").test(node));
        assertTrue(parser.parse("foo=bar&c>2").test(node));
        assertTrue(parser.parse("foo=tar | foo=bar").test(node));
        assertTrue(parser.parse("a > 0 & a < 10").test(node));
        assertFalse(parser.parse("a=1&b>2").test(node));
        assertFalse(parser.parse("a!=1|c<3").test(node));
    }

}