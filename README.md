# JTree

To simplify working with data.

Features include:

1. JSON
2. XML
3. HTTP
4. Reflection
5. Querying

...and any other tree-related stuff.

## Querying

We use something of a subset of XPath for querying nodes.  The 
intent of the language is to be a simple means for navigating 
large trees that can be embedded in URLs.

JTreePath query syntax:

| operation  | syntax                    |
|------------|---------------------------|
| descendant | `//`                      |
| child      | `/`                       |
| name       | `<string>`                |
| search     | `[<query>]`               |
| query      | `foo=bar&size>=23`        |

Examples:


| description                | syntax                    |
|----------------------------|---------------------------|
| get child nodes            | `/`                       |
| get child named "foo"      | `/foo`                    |
| get descendants named "foo | `//foo`                   |
| get users over 40          | `/users[age>40]`          |